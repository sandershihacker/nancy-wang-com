---
layout: post
author: Nancy Wang
---
So it’s been like 3 or 4 months and I’ve been ruminating (this word
makes me think of cows for some reason and I’ve decided I like it)
about probabilities again, especially since the recent passing of a
fellow peer at college.

Mostly, though, I’ve been thinking about serendipity. Math is
fascinating, because of how easily we can quantify the likelihood of
events, the way we can predict the results of experiments and processes
that may happen, or may not happen– and that’s ok, because with math we
can chase our imagination with hard numbers and data.

But math is not a fortune teller. Within any moment in time, math can
tell us the chances of one event happening over another, but it cannot
tell which will happen, won’t happen, or was so close (!) to happening.

And so: serendipity. We never try to seek or predict or calculate it, yet
we can find it anyway. Isn’t it beautiful– the way we step into the realm
of unexpectedness and uncertainty– and in it gain happiness and joy of
new dimensions?

I thought I had something more to say here besides that serendipity is
possibly my favorite word in the English language. But alas I have lost
all concentration on this topic and can’t find a solution (ha someone
please appreciate this chemistry joke).